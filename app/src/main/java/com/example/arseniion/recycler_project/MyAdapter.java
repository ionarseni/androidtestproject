package com.example.arseniion.recycler_project;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by arseni.ion on 09.08.2017.
 */

public class MyAdapter extends  RecyclerView.Adapter<MyAdapter.MyViewHolder>{
        Context context;
    List<RecyclerItem> listitems;

    public MyAdapter(Context context, List<RecyclerItem> listitems) {
        this.context = context;
        this.listitems = listitems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlist, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RecyclerItem item=listitems.get(position);
        holder.title.setText(item.getTitle());



    }

    @Override
    public int getItemCount() {
         return  listitems.size();
    }


    public  class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.title)
        TextView title;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
