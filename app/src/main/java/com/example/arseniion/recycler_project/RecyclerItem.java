package com.example.arseniion.recycler_project;

import android.media.Image;
import android.widget.ImageView;

/**
 * Created by arseni.ion on 09.08.2017.
 */

public class RecyclerItem {
    private String title;
    private  int arrow;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getArrow() {
        return arrow;
    }

    public void setArrow(int arrow) {
        this.arrow = arrow;
    }

    public RecyclerItem(String title, int arrow) {

        this.title = title;
        this.arrow = arrow;
    }
}